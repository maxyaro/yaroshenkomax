﻿using System;

namespace Hero
{
    public class Hero
    {
        private int _health;
        private int _mana;
        private int _damage;
        private int _attack_power;
        private int _energy;
        private int _mind;

        public int Health
        {
            get { return _health; }
            set { _health = value; }
        }
        public int Mana
        {
            get { return _mana; }
            set { _mana = value; }
        }
        public int Damage
        {
            get { return _damage; }
            set { _damage = value; }
        }
        public int Attack_power
        {
            get { return _attack_power; }
            set { _attack_power = value; }
        }
        public int Energy
        {
            get { return _energy; }
            set { _energy = value; }
        }
        public int Mind
        {
            get { return _mind; }
            set { _mind = value; }
        }

        public void DamageHealth(Hero userhero)
        {
            Health--;
        }
        public void HealthHealth()
        {
            Health++;
        }
        public void ManaCosts(Hero userhero)
        {
            Mana--;
        }
        public void ManaRegeneration()
        {
            Mana++;
        }
        public void DamagePlus()
        {
            Damage++;
        }
        public void DamageMinus()
        {
            Damage--;
        }
        public void Attack_powerPlus(Hero userhero)
        {
            _attack_power ++;
        }
        public void EnergyMinus()
        {
            Energy--;
        }
        public void EnergyPlus()
        {
            Energy++;
        }
        public void MindPlus()
        {
            Mind++;
        }
        public void MindMinus()
        {
            Mind--;
        }
        public void CheckDead()
        {
            if (Health <= 0)
            {
                Console.WriteLine($"You dead...");
            }
        }

        public void Enemy1() //lab_2
        {
            Health -= 3;
            _mana += 30;
            if (Health > 0)
                Console.WriteLine($"You won First Enemy!");
            else
                Console.WriteLine($"You dead...");
        }

        public override string ToString()
        {
            return $"Health: {Health} Mana: {Mana} Damage:{Damage} Attack_power: {_attack_power} Energy: { _energy} Mind {Mind}";
        }
    }
    public class Warrior : Hero //lab_3
    {
        private int _strength;

        public int Strength
        {
            get { return _strength; }
            set { _strength = value; }
        }

        public new int Damage { get; private set; }

        public void FireBall(Hero hero)
        {
            if (Strength > 0)
                hero.Health = hero.Health - Damage * 4;
        }
    }
}
