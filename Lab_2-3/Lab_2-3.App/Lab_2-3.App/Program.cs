﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hero
{
    class Program
    {
        static void Main(string[] args)
        {
            Hero userhero = new Hero
            {
                Health = 10,
                Mana = 10,
                Damage = 15,
                Energy = 1,
                Attack_power = 2
            };

            Hero enemy1hero = new Hero
            {
                Health = 20,
                Damage = 2,
                Mind = 1
            };

            Hero enemy2hero = new Hero
            {
                Health = 25,
                Damage = 4,
                Mind = 2
            };

            bool WhileStatus = true;

            while (WhileStatus == true)
            {
                Console.WriteLine(userhero.ToString());

                StringBuilder MainChoiceMenu = new StringBuilder();
                MainChoiceMenu.AppendLine("Selection action:");
                MainChoiceMenu.AppendLine("1.Damage Heath hero.");
                MainChoiceMenu.AppendLine("2.Health up.");
                MainChoiceMenu.AppendLine("3.Mana increase .");
                MainChoiceMenu.AppendLine("4.Healing health.");
                MainChoiceMenu.AppendLine("5.Boost energy.");
                MainChoiceMenu.AppendLine("6.Energy reduction.");
                MainChoiceMenu.AppendLine("7.Plus Expirience.");
                MainChoiceMenu.AppendLine("8.Decreases the Hero's mind.");
                MainChoiceMenu.AppendLine($"9.Attack FirstEnemy. Health:{enemy1hero.Health}, Damage:{enemy1hero.Damage}, Level:{enemy1hero.Attack_power}");
                MainChoiceMenu.AppendLine($"10.Attack SecondEnemy. Health:{enemy2hero.Health}, Damage:{enemy2hero.Damage}, Level:{enemy2hero.Attack_power}");
                Console.Write(MainChoiceMenu);

                Console.WriteLine("Enter number: ");
                int num = Convert.ToInt32(Console.ReadLine());
                switch (num)
                {
                    case 1:
                        userhero.DamagePlus();
                        Console.WriteLine(userhero.ToString());
                        break;
                    case 2:
                        userhero.HealthHealth();
                        Console.WriteLine(userhero.ToString());
                        break;
                    case 3:
                        userhero.ManaRegeneration();
                        Console.WriteLine(userhero.ToString());
                        break;
                    case 4:
                        userhero.DamageMinus();
                        Console.WriteLine(userhero.ToString());
                        break;
                    case 5:
                        userhero.EnergyPlus();
                        Console.WriteLine(userhero.ToString());
                        break;
                    case 6:
                        userhero.EnergyMinus();
                        Console.WriteLine(userhero.ToString());
                        break;
                    case 7:
                        userhero.MindPlus();
                        Console.WriteLine(userhero.ToString());
                        break;
                    case 8:
                        userhero.MindMinus();
                        Console.WriteLine(userhero.ToString());
                        break;
                    default:
                        Console.WriteLine("Error, try again.");
                        break;
                    case 9:
                        Console.WriteLine($"How you want attak enemy? 1 - Default 2 - Skill");
                        int i = Convert.ToInt32(Console.ReadLine());
                        if (i == 1)
                        {
                            userhero.DamageHealth(enemy1hero);
                            enemy1hero.DamageHealth(userhero);
                            Console.WriteLine(userhero.ToString());
                            Console.WriteLine(enemy1hero.ToString());
                        }
                        else if (i == 2)
                        {
                            userhero.DamageHealth(enemy1hero);
                            userhero.Attack_powerPlus(userhero);

                        }
                        if (enemy1hero.Health <= 0)
                        {
                            Console.WriteLine($"Enemy hero is dead!");
                            Console.ReadKey();
                        }
                        break;
                    case 10:
                        Console.WriteLine($"How you want attak enemy? 1 - Default 2 - Skill");
                        int x = Convert.ToInt32(Console.ReadLine());
                        if (x == 1)
                        {
                            userhero.DamageHealth(enemy2hero);
                            enemy2hero.DamageHealth(userhero);
                        }
                        else if (x == 2)
                        {
                            userhero.DamageHealth(enemy2hero);
                            userhero.ManaCosts(userhero);

                        };
                        if (enemy2hero.Health <= 0)
                        {
                            Console.WriteLine($"Enemy hero is dead!");
                            Console.ReadKey();
                        }
                        break;
                }
                Console.Clear();

                if (userhero.Health <= 0)
                {
                    WhileStatus = false;
                    userhero.CheckDead();
                    Console.WriteLine(userhero.ToString());
                }
            }
        }
    }
}