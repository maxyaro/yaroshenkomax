﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hero
{
    public class Boss : AbstractClass
    {
        public Boss(string name)
        {
            name_ = Name;
            Attack_power = 50;
            Mind = 10;
        }
        private int attack_power_;
        private string name_;
        private int mind_;

        public override int Attack_power
        {
            get
            {
                return attack_power_;
            }
            set
            {
                attack_power_ = value;

            }
        }

        public override string Name
        {
            get
            {
                return name_;
            }
            set
            {
                name_ = value;
            }
        }
        public override int Mind
        {
            get
            {
                return mind_;
            }
            set
            {
                mind_ = value;
            }
        }

        public override void Attack(AbstractClass enemy)
        {
            enemy.Attack_power--;
        }

        public override void Healed()
        {
            Attack_power++;
        }

        public override void LevelUp()
        {
            Mind++;
        }
        public override string ToString()
        {
            return $"Name: {Name}\nHealth: {Attack_power}\nExperience:";
        }
    }
}
