﻿using System;
using System.Collections.Generic;

namespace Hero
{
    public class Hero : AbstractClass
    {
        private int attack_power_;
        private int energy_;
        private string name_;


        public Hero(string name)
        {
            name_ = name;
            Attack_power = 100;
            Mind = 15;
        }

        public override string Name
        {
            get
            {
                return name_;
            }
            set
            {
                name_ = value;
            }
        }
        public override int Attack_power
        {
            get
            {
                return attack_power_;
            }
            set
            {
                attack_power_ = value;
            }

        }
        public override int Mind
        {
            get
            {
                return energy_;
            }
            set
            {
                energy_ = value;
            }

        }

        public override void Attack(AbstractClass enemy)
        {
            enemy.Attack_power--;
        }
        public override void Healed()
        {
            Attack_power++;

        }
        public override void LevelUp()
        {
            Mind++;
        }
        public override string ToString()
        {
            return $"Name: {Name}\nHealth: {Attack_power}\nExperience:";
        }

        
    }

}
