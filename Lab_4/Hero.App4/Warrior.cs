﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hero
{
    public class Warrior : AbstractClass, Interface
    {
        private int strength__;
        private int energy_;
        private string name_;


        public Warrior(string name)
        {
            name_ = name;
            Attack_power = 1255;
            Mind = 0;
        }

        public override string Name
        {
            get
            {
                return name_;
            }
            set
            {
                name_ = value;
            }
        }
        public override int Attack_power
        {
            get
            {
                return strength__;
            }
            set
            {
                strength__ = value;
            }

        }
        public override int Mind
        {
            get
            {
                return energy_;
            }
            set
            {
                energy_ = value;
            }
        }
        public override void Attack(AbstractClass enemy)
        {
            enemy.Attack_power--;
        }
        public override void Healed()
        {
            Attack_power++;

        }

        public void HyperAttack(AbstractClass enemy)
        {
            Attack_power -= 110;
            enemy.Attack_power -= 5;
        }


        public override void LevelUp()
        {
            Mind++;
        }

        public override string ToString()
        {
            return $"Name: {Name}\nHealth: {Attack_power}\nExperience:";
        }

        
    }

}
