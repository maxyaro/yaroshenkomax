﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hero
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Name of your hero: ");
            string name = Console.ReadLine();
            AbstractClass Max = new Hero(name);
            AbstractClass Yaro = new Boss(name);
            AbstractClass Cobe = new Warrior(name);
            Console.WriteLine("Type any of specified commands for work:");
            bool work = true;
            while (work)
            {
                string option = Console.ReadLine();
                switch (option)
                {
                    case "attack":
                        Max.Attack();
                        Console.WriteLine($"{Max.Name} attacked. Strength reduced");
                        break;
                    case "healed":
                        Max.Healed();
                        Console.WriteLine($"{Max.Name} healed ");
                        break;
                    case "levelup":
                        Max.LevelUp();
                        Console.WriteLine($"{Max.Name} level upped");
                        break;
                    case "stats":
                        Console.WriteLine(Max);
                        break;
                    case "exit":
                        work = false;
                        break;
                    default:
                        Console.WriteLine("Error");

                        break;
                }

            }

            Console.ReadKey();
        }


    }
}
