﻿using System;

namespace Robocop
{
    public class Robocop
    {
        private int _health;
        private int _mana;
        private int _damage;
        private int _skill;
        private int _attack_power;
        private int _energy;
        private int _mind;

        public int Health
        {
            get { return _health; }
            set { _health = value; }
        }
        public int Mana
        {
            get { return _mana; }
            set { _mana = value; }
        }
        public int Damage
        {
            get { return _damage; }
            set { _damage = value; }
        }
        public int Skill
        {
            get { return _skill; }
            set { _skill = value; }
        }
        public int Attack_power
        {
            get { return _attack_power; }
            set { _attack_power = value; }
        }
        public int Energy
        {
            get { return _energy; }
            set { _energy = value; }
        }
        public int Mind
        { get { return _mind; }
          set{ _mind = value; }
            } 

        public void DamageHealth()
        {
            Health--;
        }
        public void HealthHealth()
        {
            Health++;
        }
        public void ManaCosts()
        {
            Mana--;
        }
        public void ManaRegeneration()
        {
            Mana++;
        }
        public void DamagePlus()
        {
            Damage++;
        }
        public void DamageMinus()
        {
            Damage--;
        }
        public void LevelUp()
        {
            Skill++;
        }
        public void Attack_powerPlus()
        {
            _attack_power += 15;
        }
        public void EnergyMinus()
        {
            Energy--;
        }
        public void EnergyPlus()
        {
            Energy++;
        }
        public void MindPlus()
        {
            Mind++;
        }
        public void MindMinus()
        {
            Mind--;
        }
        public override string ToString()
        {
            return $"Health: {Health} Mana: {Mana} Damage:{Damage} Skill: {Skill} Attack_power: {_attack_power} Energy: { _energy} Mind {Mind}";
        }
    }
}