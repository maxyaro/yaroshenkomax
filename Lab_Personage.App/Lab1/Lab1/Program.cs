﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robocop
{
    class Program
    {
        static void Main(string[] args)
        {
            Robocop UserHero = new Robocop
            {
                Health = 20,
                Mana = 20,
                Damage = 5,
                Skill = 2,
                Attack_power = 5,
                Energy = 10,
                Mind = 20
            };
            int choice;
            do
            {
                Console.WriteLine($"");
                Console.WriteLine(UserHero.ToString());

                Console.WriteLine("Enter number: ");
                int num = Convert.ToInt32(Console.ReadLine());
                switch (num)
                {
                    case 1:
                        UserHero.DamageHealth();
                        Console.WriteLine(UserHero.ToString());
                        break;
                    case 2:
                        UserHero.ManaCosts();
                        Console.WriteLine(UserHero.ToString());
                        break;
                    case 3:
                        UserHero.DamagePlus();
                        Console.WriteLine(UserHero.ToString());
                        break;
                    case 4:
                        UserHero.HealthHealth();
                        Console.WriteLine(UserHero.ToString());
                        break;
                    case 5:
                        UserHero.ManaRegeneration();
                        Console.WriteLine(UserHero.ToString());
                        break;
                    case 6:
                        UserHero.DamageMinus();
                        Console.WriteLine(UserHero.ToString());
                        break;
                    case 7:
                        UserHero.Attack_powerPlus();
                        Console.WriteLine(UserHero.ToString());
                        break;
                    case 8:
                        UserHero.LevelUp();
                        Console.WriteLine(UserHero.ToString());
                        break;
                    case 9:
                        UserHero.EnergyPlus();
                        Console.WriteLine(UserHero.ToString());
                        break;
                    case 10:
                        UserHero.EnergyMinus();
                        Console.WriteLine(UserHero.ToString());
                        break;
                    case 11:
                        UserHero.MindPlus();
                        Console.WriteLine(UserHero.ToString());
                        break;
                    case 12:
                        UserHero.MindMinus();
                        Console.WriteLine(UserHero.ToString());
                        break;
                    default:
                        Console.WriteLine("Error, try again.");
                        break;
                }

                Console.WriteLine("If you want to continue, press -1");
                choice = Convert.ToInt32(Console.ReadLine());
            }
            while (choice == 1);
        }
    }
}